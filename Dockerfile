# Start with a base image containing Java runtime
FROM openjdk:10-jre-slim
#x86_64-alpine-jdk-10.0.2.13-slim

# Add Maintainer Info
LABEL maintainer="marek@mfconsulting.pl"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8090 available to the world outside this container
EXPOSE 8090

# The application's jar file
ARG JAR_FILE=build/libs/lideoapp-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} lideo-app.jar

# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/lideo-app.jar"]