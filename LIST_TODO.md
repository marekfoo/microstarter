This is a list of TODO tasks for every one who want to participate.
Feel free what you want by assign task with sufficient comment.

- TODO - tests for all method in PersonMapper mapper
- TODO - tests for builders
- TODO - service for reading config properties. File is not created yet.
- TODO - missing repository|service|REST for contact entity
- DONE - Unit Testing with @WebMvcTest and Spock - by Marek Furmaniak
- DONE - spring security - by Marek Furmaniak
- DONE - on click deployment script - by Marek Furmaniak
- DONE - missing repository|service|REST for address entity
- DONE - add aspect logging to all REST methods - bym Marek Furmaniak
- DONE - handle 404 page. Controller is ready but is not working. - by Marek Furmaniak
- DONE - customize spring boot jetty server by dedicated properties - by Marek Furmaniak
- DONE - all integration tests for REST api - by Marek Furmaniak
- DONE - add configuration properties for connection pool -  by Marek Furmaniak
- DONE - improve error response, reduce stack trace to only body error -  by Marek Furmaniak
- DONE - JMeter - basic scenarios - by Marek Furmaniak
- DONE - connect sonar cloud - by Marek Furmaniak
- DONE - Pipeline for sonar and gradle - by Marek Furmaniak
