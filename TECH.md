# Technology used in this project:

## Objects
Builder object is a separate public class, which expose interface to create and edit certain object
in correct manner. The rule o thumb is each object has to provide a constructor with minimum required
parameters, which identify this object in business unique way.

### Entities
Responsible for representing tables. After object creation, they always have to be in a valid state.
This is achieved by using builders and validators.

### Transport Objects
Responsible for communication between external systems. The state of the object has to be completed and
therefore the builder pattern is used to achieved it. As well stored values have to pass validation.

### Value objects
Because they are immutable, without identity, a factory method is used to create these kind of objects.
It guarantees that after creation, object is in completed state and ready to use.
Necessary validation taking place as well. Optimization for unnecessary object creation is in place.

## Spring bean registration
Whenever some bean has to be added to spring context, it should be done via one configuration class only.
In that way, all configurations are in one place.

## Exception handling
### Business exceptions
Each exception which is thrown by application has to be catch by exception handler which in return will
create response error in dedicated json format.

### Error page
In case of server error or resource not found, custom pages has been created.

## Connectivity

## VM args
As every application can be customized at startup by using VM args, Micro-starter does it so.

### Profiles
You can choose between two profiles: dev|prod
-Dspring.profiles.active=dev

### Memory allocations
-Xms128m -Xmx512m

### Heap Dump
It’s extremely hard to diagnose any memory problems without capturing heap dumps at right time.
-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/tmp/heapdump.bin


## Diagnostics
You can diagnose app without external systems by using ready prepared API

### Threads info
It is accessible under REST /starter/threadinfo

### Spring Boot actuator
Monitoring our app, gathering metrics, understanding traffic or the state of database.
It is enabled for all out of the box endpoints. Config can be found in application.properties.
Endpoints like actuator/health or actuator/threaddump

## Loggers
Logback has been used as implementation for logger for this app. It has been configured with two
profiles dev/prod in xml file.

## Tests

### Integration tests
Repository tests has been created to verify correction of sql statements.
End-to-end tests which tests REST API has been created as well.

### Business logic tests
Unit tests, using junit and mockito framework are for business logic tests.

### Additional facility
It is possible to perform one test scenario in concurrent mode, by many threads at once.
Cleanup of database after each run of test case has been implemented as well.

