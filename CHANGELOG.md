# History of changes, new features

## Version 0.1
- Validators for controllers and entities
- Exception handling for REST controllers
- Custom error pages for 500|404|401
- System monitoring by actuator and custom controllers