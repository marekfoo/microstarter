package pl.mfconsulting.java.microstarter.controller.person.impl;

import org.junit.Test;
import org.springframework.http.MediaType;
import pl.mfconsulting.java.microstarter.configuration.WebAppConfigurationSetup;
import pl.mfconsulting.java.microstarter.controller.to.person.PersonTOBuilder;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;
import pl.mfconsulting.java.microstarter.tools.web.JsonHelper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.mfconsulting.java.microstarter.controller.person.impl.PersonURLMapping.PERSONS_URL;
import static pl.mfconsulting.java.microstarter.controller.person.impl.PersonURLMapping.PERSON_ID_URL;

public class PersonControllerIntegrationTest extends WebAppConfigurationSetup {

    @Test
    public void whenRequestGetPersonsThenReturnThem() throws Exception {
        //given
        var person1 = new PersonBuilder(Login.of("Per1")).firstName("NameTest1").lastName("SurrTest1").build();
        personService.addPerson(person1);
        var person2 = new PersonBuilder(Login.of("Per2")).firstName("NameTest2").lastName("SurrTest2").build();
        personService.addPerson(person2);

        //when
        var response = this.mockMvc.perform(get(PERSONS_URL));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(content().string(containsString(person1.getLogin().value())));
        //and
        response.andExpect(content().string(containsString(person2.getFirstName())));
    }

    @Test
    public void whenRequestGetPersonThenReturnIt() throws Exception {
        //given
        var person = new PersonBuilder(Login.of("Per3")).firstName("NameTest3").lastName("SurrTest3").build();
        personService.addPerson(person);

        //when
        var response = this.mockMvc.perform(get(PERSON_ID_URL, "Per3"));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.login", is(person.getLogin().value())));
        //and
        response.andExpect(jsonPath("$.firstName", is(person.getFirstName())));
    }

    @Test
    public void whenRequestGetPersonNotExistingThenReturnEmptyBody() throws Exception {
        //given
        addressService.deleteAddresses();
        personService.deletePersons();

        //when
        var response = this.mockMvc.perform(get(PERSON_ID_URL, "login", "Per4"));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.length()").value(4L));
        //and
        response.andExpect(jsonPath("$.login", is("")));
    }

    @Test
    public void whenRequestGetPersonsNotExistingThenReturnEmptyBody() throws Exception {
        //given
        addressService.deleteAddresses();
        personService.deletePersons();

        //empty db

        //when
        var response = this.mockMvc.perform(get(PERSONS_URL));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(content().string(containsString(EMPTY_PERSONS_BODY)));
    }

    @Test
    public void whenRequestPostPersonThenCreateIt() throws Exception {
        //given
        var personToAdd = new PersonTOBuilder("Per5").firstName("NameTest5").lastName("NameTest5").build();

        var personBody = JsonHelper.getJsonString(personToAdd);

        //when
        var response = this.mockMvc.perform(post(PERSONS_URL).content(personBody).
                contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        //then
        response.andExpect(status().isCreated()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.login", is(personToAdd.getLogin())));
        //and
        response.andExpect(jsonPath("$.firstName", is(personToAdd.getFirstName())));
    }

    @Test
    public void whenRequestPutPersonThenUpdateIt() throws Exception {
        //given
        var person1 = new PersonBuilder(Login.of("Per6")).firstName("NameTest6").lastName("SurrTest6").build();
        personService.addPerson(person1);

        var person2 = new PersonTOBuilder(person1.getLogin().value()).firstName("NewName").lastName("NewLast").build();

        String personBody = JsonHelper.getJsonString(person2);

        //when
        var response = this.mockMvc.perform(put(PERSON_ID_URL, person1.getLogin().value()).content(personBody).
                contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        //then
        response.andDo(print()).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.login", is(person1.getLogin().value())));
        //and
        response.andExpect(jsonPath("$.firstName", is(person2.getFirstName())));
        //and
        response.andExpect(jsonPath("$.lastName", is(person2.getLastName())));
    }

    @Test
    public void whenRequestDeletePersonThenDeleteIt() throws Exception {
        //given
        var person1 = new PersonBuilder(Login.of("Per7")).firstName("NameTest7").lastName("SurrTest7").build();
        personService.addPerson(person1);

        //when
        var response = this.mockMvc.perform(delete(PERSON_ID_URL, person1.getLogin().value()).
                contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        //then
        response.andDo(print()).andExpect(status().isNoContent());
        //and
        assertThat(personService.getAllPersons().size(), is(0));
    }
}