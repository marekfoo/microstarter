package pl.mfconsulting.java.microstarter.controller.address;

import org.junit.Test;
import org.springframework.http.MediaType;
import pl.mfconsulting.java.microstarter.configuration.WebAppConfigurationSetup;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressTOBuilder;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;
import pl.mfconsulting.java.microstarter.tools.web.JsonHelper;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.mfconsulting.java.microstarter.controller.address.AddressURL.PERSON_ADDRESS_URL;

public class AddressControllerIntegrationTest extends WebAppConfigurationSetup {

    @Test
    public void whenRequestPutClientWithAddressThenCreateIt() throws Exception {
        //given
        var client1 = new PersonBuilder(Login.of("Per6")).firstName("NameTest6").lastName("SurrTest6").build();
        personService.addPerson(client1);

        var clientAddress = new AddressTOBuilder().city("city6").street("street6").postalCode("postalCode6").build();
        var addressBody = JsonHelper.getJsonString(clientAddress);

        //when
        var response = this.mockMvc.perform(put(PERSON_ADDRESS_URL, client1.getLogin().value()).content(addressBody).
                contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.street", is(clientAddress.getStreet())));
        //and
        response.andExpect(jsonPath("$.city", is(clientAddress.getCity())));
        //and
        response.andExpect(content().string(containsString(clientAddress.getPostalCode())));
    }

    @Test
    public void whenRequestPutClientWithAddressAndNoClientThenNoCreateIt() throws Exception {
        //given
        var client1 = new PersonBuilder(Login.of("Per6")).firstName("NameTest6").lastName("SurrTest6").build();

        var clientAddress = new AddressTOBuilder().city("city6").street("street6").postalCode("postalCode6").build();
        var addressBody = JsonHelper.getJsonString(clientAddress);

        //when
        var response = this.mockMvc.perform(put(PERSON_ADDRESS_URL, client1.getLogin().value()).content(addressBody).
                contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.street", is("")));
        //and
        response.andExpect(jsonPath("$.city", is("")));
        //and
        response.andExpect(jsonPath("$.postalCode", is("")));
    }
}