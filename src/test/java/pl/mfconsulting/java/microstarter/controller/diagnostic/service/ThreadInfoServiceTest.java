package pl.mfconsulting.java.microstarter.controller.diagnostic.service;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

public class ThreadInfoServiceTest {

    @Test
    public void whenGetCurrentRunningThreadsThenReturnThem() {
        //given

        //when
        var threads = ThreadInfoService.getThreadInfo();

        //then
        assertThat(threads.size(), greaterThan(0));
    }
}