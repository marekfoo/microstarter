package pl.mfconsulting.java.microstarter.controller.to.person;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class PersonMapperTest {

    @Test
    public void whenPersonTOThenMapWholeObjectToPerson() {
        //given
        var personBeforeMapping = new PersonTOBuilder("person").firstName("first1").lastName("last1").build();

        //when
        var personAfterMapping = PersonMapper.toPerson(personBeforeMapping);

        //then
        assertThat(personAfterMapping.getLogin().value(), is(personBeforeMapping.getLogin()));
        //and
        assertThat(personAfterMapping.getFirstName(), is(personBeforeMapping.getFirstName()));
        //and
        assertThat(personAfterMapping.getLastName(), is(personBeforeMapping.getLastName()));
    }

    @Test
    public void whenPersonThenMapItToWholeObjectPersonTO() {
    }

    @Test
    public void whenPersonsThenMapItToWholeObjectPersonsTO() {
    }

    @Test
    public void whenPersonsTOThenMapWholeObjectToPersons() {
    }
}