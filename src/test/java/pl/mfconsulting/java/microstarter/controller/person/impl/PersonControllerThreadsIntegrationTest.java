package pl.mfconsulting.java.microstarter.controller.person.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import pl.mfconsulting.java.microstarter.configuration.WebAppConfigurationSetup;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;
import pl.mfconsulting.java.microstarter.repository.PersonRepository;
import pl.mfconsulting.java.microstarter.tools.rule.AfterThreadTest;
import pl.mfconsulting.java.microstarter.tools.rule.BeforeThreadTest;
import pl.mfconsulting.java.microstarter.tools.rule.ThreadCounter;
import pl.mfconsulting.java.microstarter.tools.rule.ThreadExecutorRule;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static pl.mfconsulting.java.microstarter.controller.person.impl.PersonURLMapping.PERSON_ID_URL;

public class PersonControllerThreadsIntegrationTest extends WebAppConfigurationSetup {

    @Rule
    public ThreadExecutorRule threadExecutorRule = new ThreadExecutorRule(this);

    @Autowired
    private PersonRepository personRepository;

    private Person person;

    @After
    @Override
    public void tearDown() {
    }

    @AfterThreadTest
    public void tearDownDB() {
        personRepository.deleteAll();
    }

    @BeforeThreadTest
    public void setUpDb() {
        person = new PersonBuilder(Login.of("Per3")).firstName("NameTest3").lastName("SurrTest3").build();

        personRepository.findByLogin(person.getLogin()).ifPresentOrElse(System.out::println, () -> personRepository.save(person));
    }

    @Test
    @ThreadCounter
    public void whenRequestGetPersonThenReturnIt() throws Exception {
        //given

        //when
        var response = this.mockMvc.perform(get(PERSON_ID_URL, person.getLogin().value()));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.login", is(person.getLogin().value())));
        //and
        response.andExpect(jsonPath("$.firstName", is("NameTest3")));
    }

    @Test
    @ThreadCounter
    public void whenRequestDifferentGetPersonThenReturnThem() throws Exception {
        //given
        var rand = RandomStringUtils.random(10, false, true);
        var login = Login.of("Per3" + rand);
        var person = new PersonBuilder(login).firstName("NameTest3").lastName("SurrTest3").build();
        personRepository.save(person);

        //when
        var response = this.mockMvc.perform(get(PERSON_ID_URL, login.value()));

        //then
        response.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        //and
        response.andExpect(jsonPath("$.login", is(login.value())));
        //and
        response.andExpect(jsonPath("$.firstName", is("NameTest3")));
    }
}