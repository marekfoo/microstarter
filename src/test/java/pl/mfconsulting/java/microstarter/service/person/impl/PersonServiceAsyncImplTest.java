package pl.mfconsulting.java.microstarter.service.person.impl;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;
import pl.mfconsulting.java.microstarter.service.person.PersonService;
import pl.mfconsulting.java.microstarter.tools.rule.BeforeThreadTest;
import pl.mfconsulting.java.microstarter.tools.rule.ThreadCounter;
import pl.mfconsulting.java.microstarter.tools.rule.ThreadExecutorRule;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceAsyncImplTest {

    @Mock
    private static PersonService personService;
    @Rule
    public ThreadExecutorRule threadExecutorRule = new ThreadExecutorRule(this);
    @InjectMocks
    private PersonServiceAsyncImpl serviceAsyncImpl;
    private Person person;

    @BeforeThreadTest
    public void initMockedDB() {
        person = new PersonBuilder(Login.of("Per3")).firstName("NameTest3").lastName("SurrTest3").build();

        when(personService.findPersonByLogin(person.getLogin())).thenReturn(Optional.of(person));
    }

    @Test
    @ThreadCounter
    public void whenFindPersonByLoginThenReturnIt() throws ExecutionException, InterruptedException {
        //given

        //when
        CompletableFuture<Optional<Person>> foundPerson = serviceAsyncImpl.findPersonByLogin(person.getLogin());

        //then
        var asyncPerson = foundPerson.get().orElse(Person.EMPTY);

        assertNotNull(asyncPerson);
        //and
        assertThat(asyncPerson.getLogin(), equalTo(person.getLogin()));
        //and
        assertThat(asyncPerson.getFirstName(), equalTo(person.getFirstName()));
        //and
        assertThat(asyncPerson.getLastName(), equalTo(person.getLastName()));
    }

    @Test(expected = BusinessRuntimeException.class)
    @ThreadCounter
    public void whenPersonNullByLoginThenThrowException() {
        //given

        //when
        serviceAsyncImpl.findPersonByLogin(null);

        //then
        fail();
    }

    @Test
    @ThreadCounter
    public void whenPersonEmptyByLoginThenThrowException() {
        //given

        //when
        serviceAsyncImpl.findPersonByLogin(Login.of("NotExisting"));

        //then
        assertTrue(true);
    }
}