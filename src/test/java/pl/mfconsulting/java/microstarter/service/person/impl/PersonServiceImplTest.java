package pl.mfconsulting.java.microstarter.service.person.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;
import pl.mfconsulting.java.microstarter.repository.AddressRepository;
import pl.mfconsulting.java.microstarter.repository.PersonRepository;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PersonServiceImplTest {

    @InjectMocks
    private PersonServiceImpl personService;

    @Mock
    private PersonRepository personRepository;
    @Mock
    private AddressRepository addressRepository;

    @Before
    public void setup() {
        personService = new PersonServiceImpl(personRepository, addressRepository);
    }

    @Test
    public void whenAskForPersonThenReturnIt() {
        //given
        var login = Login.of("alex");
        when(personRepository.findByLogin(login)).thenReturn(Optional.of(new PersonBuilder(login).build()));

        //when
        var found = personService.findPersonByLogin(login);

        //then
        var foundPerson = found.orElse(Person.EMPTY);

        assertThat(foundPerson.getLogin(), is(login));
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenAddPersonAndItExistsThenThrowException() {
        //given
        var login = Login.of("alex");
        var person = new PersonBuilder(login).firstName("aaa").lastName("bbb").build();

        when(personRepository.findByLogin(login)).thenReturn(Optional.of(person));

        //when
        personService.addPerson(person);

        //then
        fail();
    }

    @Test
    public void whenNoPersonThenAddIt() {
        //given
        var login = Login.of("alex");
        var person = new PersonBuilder(login).firstName("aaa").lastName("bbb").build();

        when(personRepository.findByLogin(login)).thenReturn(Optional.empty());
        when(personRepository.save(person)).thenReturn(person);

        //when
        var addedPerson = personService.addPerson(person);

        var personResult = addedPerson.orElse(Person.EMPTY);

        assertThat(personResult.getLogin(), equalTo(person.getLogin()));
        //and
        assertThat(personResult.getFirstName(), equalTo(person.getFirstName()));
        //and
        assertThat(personResult.getLastName(), equalTo(person.getLastName()));
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenEditPersonNotFoundThenThrowException() {
        //given
        var login = Login.of("alex");
        var person = new PersonBuilder(login).firstName("aaa").lastName("bbb").build();

        when(personRepository.findByLogin(login)).thenReturn(Optional.empty());

        //when
        personService.editPerson(login, person);

        //then
        fail();
    }

    @Test
    public void whenPersonExistsThenEditIt() {
        //given
        var login = Login.of("alex");
        var person = new PersonBuilder(login).firstName("aaa").lastName("bbb").build();
        var personEdited = new PersonBuilder(login).firstName("newName").lastName("newLastName").build();

        when(personRepository.findByLogin(login)).thenReturn(Optional.of(person));
        when(personRepository.save(personEdited)).thenReturn(personEdited);

        //when
        var result = personService.editPerson(login, personEdited);

        //then
        var personResult = result.orElse(Person.EMPTY);

        assertThat(personResult.getLogin(), equalTo(person.getLogin()));
        //and
        assertThat(personResult.getFirstName(), equalTo(personEdited.getFirstName()));
        //and
        assertThat(personResult.getLastName(), equalTo(personEdited.getLastName()));
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenDeletePersonAndNotFoundThenThrowException() {
        //given
        var login = Login.of("alex");
        var person = new PersonBuilder(login).firstName("aaa").lastName("bbb").build();

        when(personRepository.findByLogin(login)).thenReturn(Optional.empty());

        //when
        personService.deletePerson(login);

        //then
        fail();
    }

    @Test
    public void whenPersonExistsThenDeleteIt() {
        //given
        var login = Login.of("alex");
        var person = new PersonBuilder(login).firstName("aaa").lastName("bbb").build();

        when(personRepository.findByLogin(login)).thenReturn(Optional.of(person));

        //when
        personService.deletePerson(login);

        //then
        verify(personRepository).delete(person);
    }

}
