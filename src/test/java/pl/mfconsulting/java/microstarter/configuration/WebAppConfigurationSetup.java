package pl.mfconsulting.java.microstarter.configuration;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import pl.mfconsulting.java.microstarter.service.address.AddressService;
import pl.mfconsulting.java.microstarter.service.person.PersonService;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"test"})
@EnableAsync
public abstract class WebAppConfigurationSetup {

    protected static final String EMPTY_PERSONS_BODY = "{\"persons\":[]}";

    protected MockMvc mockMvc;

    @Autowired
    protected PersonService personService;

    @Autowired
    protected AddressService addressService;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @After
    public void tearDown() {
        addressService.deleteAddresses();
        personService.deletePersons();
    }
}
