package pl.mfconsulting.java.microstarter.persistence.person;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

@RunWith(ConcurrentTestRunner.class)
public class LoginTest {

    @BeforeClass
    public static void setup() {
        Login.logins.clear();
    }

    @Test
    @ThreadCount(10)
    public void whenCreateLoginWithTheSameValueThenCreateOnlyOneObject() {
        //when
        var one = Login.of("onexet");

        //then
        assertThat(Login.logins.containsKey("onexet"), is(true));
    }

    @Test
    public void whenEmptyObjectThenValueIsNull() {
        //given
        var empty = Login.EMPTY;

        //when
        var val = empty.value();

        //then
        assertNull(val);
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenValueIsTooBigThenThrowException() {
        //given
        String bigString = "012345678901234567890123456789012345678901234567891";

        //when
        var val = Login.of(bigString);

        //then
        fail();
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenValueIsEmptyThenThrowException() {
        //given
        String bigString = "";

        //when
        var val = Login.of(bigString);

        //then
        fail();
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenValueIsNullThenThrowException() {
        //given
        String bigString = null;

        //when
        var val = Login.of(bigString);

        //then
        fail();
    }

    @Test
    public void whenValueIsCorrectThenCreateLogin() {
        //given
        String login = "012345678901234567890";

        //when
        var val = Login.of(login);

        //then
        assertThat(val.value(), is(login));
    }
}