package pl.mfconsulting.java.microstarter.persistence.person;

import org.junit.Test;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class PersonBuilderTest {

    @Test(expected = BusinessRuntimeException.class)
    public void whenNameIsMoreThan50ThenThrowException() {
        //given
        var name = "1234567890123456789012345678901312345678901234567890123456789013";
        var lastName = "12345678901";

        //when
        new PersonBuilder(Login.of("123")).firstName(name).lastName(lastName).build();

        //then
        fail();
    }

    @Test(expected = BusinessRuntimeException.class)
    public void whenLastNameIsMoreThan50ThenThrowException() {
        //given
        var lastName = "1234567890123456789012345678901312345678901234567890123456789013";
        var name = "12345678901";

        //when
        new PersonBuilder(Login.of("123")).firstName(name).lastName(lastName).build();

        //then
        fail();
    }

    @Test
    public void whenLastNameAndNameIsLessThan50ThenOK() {
        //given
        var lastName = "12345678901234567890123456789";
        var name = "12345678901";

        //when
        var person = new PersonBuilder(Login.of("123")).firstName(name).lastName(lastName).build();

        //then
        assertThat(person.getFirstName(), is(name));
        //and
        assertThat(person.getLastName(), is(lastName));
    }
}