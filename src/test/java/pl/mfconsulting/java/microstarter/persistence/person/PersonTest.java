package pl.mfconsulting.java.microstarter.persistence.person;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;


public class PersonTest {

    @Test
    public void whenPersonThenSameThenReturnTrue() {
        //given
        var person1 =
                new PersonBuilder(Login.of("bolek")).firstName("nameBolek").lastName("surnameBolek").build();

        var person2 = new PersonBuilder(Login.of("bolek")).build();

        //when
        var result = person1.equals(person2);

        //then

        assertThat(result, is(Boolean.TRUE));
    }

    @Test
    public void whenEmptyObjectThenValueIsNull() {
        //given
        var empty = Person.EMPTY;

        //when
        var val = empty.getFirstName();

        //then
        assertNull(val);
    }

}