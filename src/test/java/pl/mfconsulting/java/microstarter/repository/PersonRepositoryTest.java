package pl.mfconsulting.java.microstarter.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void whenNoPersonsThenReturnEmptyList() {

        //when
        var people = personRepository.findAll();

        //then
        assertThat(people.size(), is(0));
    }

    @Test
    public void whenOnePersonThenReturnOnePerson() {
        //given
        var person =
                new PersonBuilder(Login.of("nalek")).firstName("falek").lastName("lalek").build();
        entityManager.persistAndFlush(person);

        //when
        var people = personRepository.findAll();

        //then
        assertThat(people.size(), is(1));
    }

    @Test
    public void whenLookForPersonThenReturnIt() {
        //given
        var person =
                new PersonBuilder(Login.of("nalek")).firstName("falek").lastName("lalek").build();
        var person2 =
                new PersonBuilder(Login.of("nalek2")).firstName("falek2").lastName("lalek2").build();

        entityManager.persistAndFlush(person);
        entityManager.persistAndFlush(person2);

        //when
        var foundPerson = personRepository.findByLogin(Login.of("nalek2"));

        //then
        assertThat(foundPerson.get().equals(person2), is(Boolean.TRUE));
    }

    @Test
    public void whenAddPersonThenReturnIt() {
        //given
        var person =
                new PersonBuilder(Login.of("nalek")).firstName("falek").lastName("lalek").build();

        //when
        var foundPerson = personRepository.save(person);

        //then
        assertThat(foundPerson.equals(person), is(Boolean.TRUE));
    }

    @Test
    public void whenDeletePersonThenEmptyDB() {
        //given
        var person =
                new PersonBuilder(Login.of("nalek")).firstName("falek").lastName("lalek").build();

        entityManager.persistAndFlush(person);

        //when
        personRepository.delete(person);

        //then
        assertThat(personRepository.findAll().size(), is(0));
    }
}