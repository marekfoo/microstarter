package pl.mfconsulting.java.microstarter.tools.rule;

import org.junit.Assert;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class ThreadExecutorRule extends TestWatcher {
    private final Object target;
    private Logger LOG = LoggerFactory.getLogger(ThreadExecutorRule.class);

    public ThreadExecutorRule(Object target) {
        this.target = target;
    }

    @Override
    public Statement apply(final Statement base,
                           final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                ThreadCounter threadCount = description.getAnnotation(ThreadCounter.class);

                if (threadCount == null) {
                    base.evaluate();
                    return;
                }

                try {
                    LOG.debug("Start multi-thread test. Thread counter: {}", threadCount.value());

                    callBeforeMultiThreadTest(description);

                    List<CompletableFuture<Throwable>> features = initializedAllThreads(threadCount, base);

                    checkResults(features, description);
                } finally {
                    callAfterMultiThreadTest(description);
                }
            }
        };
    }

    private List<CompletableFuture<Throwable>> initializedAllThreads(ThreadCounter threadCount, Statement base) {
        ExecutorService executor = Executors.newFixedThreadPool(threadCount.value());
        List<CompletableFuture<Throwable>> features = new ArrayList<>(threadCount.value());

        for (int i = 0; i < threadCount.value(); i++) {
            final int threadNo = i;
            final Supplier<Throwable> asyncTaskSupplier = () -> {
                LOG.debug("Initialized thread ({})", threadCount);

                try {
                    base.evaluate();
                } catch (Throwable throwable) {
                    LOG.error("Thread({}): An exception occurs: ", threadNo, throwable);
                    return throwable;
                }

                return null;
            };

            features.add(CompletableFuture.supplyAsync(asyncTaskSupplier, executor));
        }
        return features;
    }

    private void checkResults(List<CompletableFuture<Throwable>> features, Description description) {
        features.forEach(f -> {
            try {
                if (f.get() != null) {
                    ThreadExecutorRule.this.failed(f.get(), description);
                    f.get().printStackTrace();
                    Assert.fail("Test failed,  errors occurs during the multi-thread tests.");
                }
                f.get();
            } catch (Exception e) {
                e.printStackTrace();
                ThreadExecutorRule.this.failed(e, description);
            }
        });
    }

    private void callAfterMultiThreadTest(Description description) {
        for (Method method : description.getTestClass().getMethods()) {
            if (method.getAnnotation(AfterThreadTest.class) != null) {
                try {
                    method.invoke(target);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void callBeforeMultiThreadTest(Description description) {
        for (Method method : description.getTestClass().getMethods()) {
            if (method.getAnnotation(BeforeThreadTest.class) != null) {
                try {
                    method.invoke(target);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
