package pl.mfconsulting.java.microstarter.tools.web;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class JsonHelper {
    public static String getJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
