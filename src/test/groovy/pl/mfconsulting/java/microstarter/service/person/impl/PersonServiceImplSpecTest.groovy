package pl.mfconsulting.java.microstarter.service.person.impl

import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException
import pl.mfconsulting.java.microstarter.persistence.person.Login
import pl.mfconsulting.java.microstarter.persistence.person.Person
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder
import pl.mfconsulting.java.microstarter.repository.AddressRepository
import pl.mfconsulting.java.microstarter.repository.PersonRepository
import spock.lang.Specification

class PersonServiceImplSpecTest extends Specification {

    PersonRepository personRepository = Mock()

    AddressRepository addressRepository = Mock()

    def personService = new PersonServiceImpl(personRepository, addressRepository) //Spy(PersonServiceImpl, constructorArgs:[personRepository, addressRepository])

    def 'adding the same person twice will result with exception'() {
        given:
            def name = "name1"
            def login = Login.of("asfd")
            def person = new PersonBuilder(login).firstName(name).build()
            personRepository.findByLogin(login) >> Optional.of(person)

        when:
        personService.addPerson(person).orElse(Person.EMPTY)

        then:
            thrown(BusinessRuntimeException)
    }

    def 'adding the non existing person will result with success'() {
        given: "person for adding"
            def name = "name1"
            def login = Login.of("asfd")
            def person = new PersonBuilder(login).firstName(name).build()

            personRepository.findByLogin(login) >> Optional.empty()
            personRepository.save(person) >> person

        when: "adding person using service"
        def personResult = personService.addPerson(person).orElse(Person.EMPTY)

        then: "first name must much"
            personResult.getFirstName() == name
    }
}
