package pl.mfconsulting.java.microstarter.persistence.person

import spock.lang.Specification

class PersonBuilderSpecTest extends Specification {

    def "name less then 50 char is correct"() {
        given:
        def myLastName = "12345678901234567890123456789"
        def myName = "12345678901"

        when:
        def person = new PersonBuilder(Login.of("123")).firstName(myName).lastName(myLastName).build()

        then:
        with(person) {
            firstName == myName
            lastName == myLastName
        }
    }
}
