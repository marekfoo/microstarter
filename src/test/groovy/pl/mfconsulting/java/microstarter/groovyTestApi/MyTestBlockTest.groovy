package pl.mfconsulting.java.microstarter.groovyTestApi

import spock.lang.Specification

class MyTestBlockTest extends Specification {

    def "test with block"() {
        given:
        def myList = [1, 2, 3, 4]

        when:
        myList.remove(0)

        then:
        myList == [2, 3, 4]
    }

    def "when wrong index then exception"() {
        given:
        def myList = [1, 2, 3]

        when:
        myList.remove(20)

        then:
        thrown(IndexOutOfBoundsException)
        myList.size() == 3
    }

}
