package pl.mfconsulting.java.microstarter.groovyTestApi


import spock.lang.Specification

class MyClassParam3Test extends Specification {
    def 'Get the bigger number'() {

        expect: 'Should return the bigger number'
        Math.max(a, b) == c

        where:
        a | b || c
        1 | 0 || 1
        2 | 3 || 3
    }
}
