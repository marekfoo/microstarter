package pl.mfconsulting.java.microstarter.groovyTestApi

class MyClass {
    static def int max(int integer1, int integer2) {
        return integer1 > integer2 ? integer1 : integer2;
    }
}
