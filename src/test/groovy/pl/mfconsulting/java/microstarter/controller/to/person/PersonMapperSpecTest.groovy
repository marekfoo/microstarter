package pl.mfconsulting.java.microstarter.controller.to.person

import spock.lang.Specification

class PersonMapperSpecTest extends Specification {

    def "should map PersonTO to Person with all fields"() {
        given:
            def login = "login"
            def first = "first"
            def last = "last"
        def personBeforeMappingTO = new PersonTOBuilder(login).firstName(first).lastName(last).build()

        when:
        def personAfterMapping = PersonMapper.toPerson(personBeforeMappingTO)

        then:
            with(personAfterMapping) {
                login == login
                firstName == first
                lastName == last
            }
    }
}
