package pl.mfconsulting.java.microstarter.controller.person.impl

import pl.mfconsulting.java.microstarter.configuration.WebAppConfigurationSetupSpec
import pl.mfconsulting.java.microstarter.persistence.person.Login
import pl.mfconsulting.java.microstarter.persistence.person.Person
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder

class PersonControllerIntegrationSpecTest extends WebAppConfigurationSetupSpec {

    def "Returns one added person with status 200"() {

        given:
        restClient.auth.basic 'admin', 'admin'

        Person person1 = new PersonBuilder(Login.of("Per1")).firstName("NameTest1").lastName("SurrTest1").build()
        personService.addPerson(person1)

        when:
        def resp = restClient.get([path: PersonURLMapping.PERSONS_URL])

        then:
        with(resp) {
            status == 200
            contentType == "application/json"
        }

        def personsMap = resp.getData()
        with(personsMap) {
            persons[0].login == person1.getLogin().value()
            persons[0].firstName == person1.getFirstName()
            persons[0].lastName == person1.getLastName()
        }
    }

    def "Adds one person with status 201"() {
        given:
        restClient.auth.basic 'admin', 'admin'
        def reqBody = [login: 'Per1', firstName: 'NameTest1', lastName: 'LastName1']

        when:
        def resp = restClient.post(path: PersonURLMapping.PERSONS_URL,
                body: reqBody,
                requestContentType: "application/json")

        then:
        with(resp) {
            status == 201
            contentType == "application/json"
        }

        def newPerson = resp.getData()
        with(newPerson) {
            login == 'Per1'
            firstName == 'NameTest1'
            lastName == 'LastName1'
        }
    }

    def "Edit one person with status 200"() {
        given:
        restClient.auth.basic 'admin', 'admin'

        Person person1 = new PersonBuilder(Login.of("Per1")).firstName("NameTest1").lastName("SurrTest1").build()
        personService.addPerson(person1)

        def reqBody = [login: 'Per1', firstName: 'NameTest2', lastName: "LastName2"]

        when:
        def resp = restClient.put(path: PersonURLMapping.PERSONS_URL + "/Per1",
                body: reqBody,
                requestContentType: "application/json")

        then:
        with(resp) {
            status == 200
            contentType == "application/json"
        }

        def newPerson = resp.getData()
        with(newPerson) {
            login == "Per1"
            firstName == "NameTest2"
            lastName == "LastName2"
        }
    }

    def "Delete person with status 204"() {
        given:
        restClient.auth.basic 'admin', 'admin'

        Person person1 = new PersonBuilder(Login.of("Per1")).firstName("NameTest1").lastName("SurrTest1").build()
        personService.addPerson(person1)

        when:
        def resp = restClient.delete(path: PersonURLMapping.PERSONS_URL + "/Per1")

        then:
        with(resp) {
            status == 204
        }

        assert personService.getAllPersons().size() == 0
    }

    def "Delete all persons with status 204"() {
        given:
        restClient.auth.basic 'admin', 'admin'

        Person person1 = new PersonBuilder(Login.of("Per1")).firstName("NameTest1").lastName("SurrTest1").build()
        personService.addPerson(person1)
        Person person2 = new PersonBuilder(Login.of("Per2")).firstName("NameTest2").lastName("SurrTest2").build()
        personService.addPerson(person2)

        when:
        def resp = restClient.delete(path: PersonURLMapping.PERSONS_URL)

        then:
        with(resp) {
            status == 204
        }

        assert personService.getAllPersons().size() == 0
    }
}
