package pl.mfconsulting.java.microstarter.configuration

import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import pl.mfconsulting.java.microstarter.service.address.AddressService
import pl.mfconsulting.java.microstarter.service.person.PersonService
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
class WebAppConfigurationSetupSpec extends Specification {

    @Autowired
    protected PersonService personService

    @Autowired
    protected AddressService addressService

    @Shared
    def restClient = new RESTClient("http://localhost:8080")

    def cleanup() {
        addressService.deleteAddresses()
        personService.deletePersons()
    }
}
