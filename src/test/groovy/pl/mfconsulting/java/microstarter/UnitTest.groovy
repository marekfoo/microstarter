package pl.mfconsulting.java.microstarter

/**
 * This is a marker interface that identifies all Groovy specification
 * classes which contain unit tests.
 */
interface UnitTest {

}
