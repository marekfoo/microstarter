package pl.mfconsulting.java.microstarter.common.exception;

public class BusinessRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 5544269669305350117L;

    private final String message;

    public BusinessRuntimeException(final String message) {
        super(message);
        this.message = message;
    }

    public BusinessRuntimeException(final String message, final Exception ex) {
        super(message, ex);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "BusinessRuntimeException{" +
                "message='" + message + '\'' +
                '}';
    }
}
