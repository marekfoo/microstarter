package pl.mfconsulting.java.microstarter.common.constants;

public final class RegexPatterns {
    public static final String LOGIN_CHARACTERS = "\\w{1,30}";

    private RegexPatterns() {
        throw new IllegalStateException("Utility class!");
    }
}
