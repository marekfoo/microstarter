package pl.mfconsulting.java.microstarter.common.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;

import javax.validation.*;
import java.util.Set;

public final class ObjectValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectValidator.class);

    private ObjectValidator() {
    }

    public static void notNull(final Object object, final String message) {
        if (object == null) {
            LOGGER.error("Argument is null:{}", message);
            throw new BusinessRuntimeException(message);
        }
    }

    public static void validateConstraints(Object object) {
        Set<ConstraintViolation<Object>> violations = createValidator().validate(object);

        if (violations.isEmpty()) {
            return;
        }

        LOGGER.error("Number of violations:{}", violations.size());
        violations.forEach(ObjectValidator::printError);

        throw new BusinessRuntimeException("Incorrect object state!");

    }

    private static Validator createValidator() {
        Configuration<?> config = Validation.byDefaultProvider().configure();
        ValidatorFactory factory = config.buildValidatorFactory();
        Validator validator = factory.getValidator();
        factory.close();
        return validator;
    }

    private static void printError(ConstraintViolation<?> violation) {
        LOGGER.error("{} - {}", violation.getPropertyPath(), violation.getMessage());
    }
}
