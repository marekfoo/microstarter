package pl.mfconsulting.java.microstarter.common.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;


@Aspect
@Component
public class LogAspect {
    private static final String CLEAN_POINTCUT = "@annotation(pl.mfconsulting.java.microstarter.common.aspects.LogMethod)";
    private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

    @Pointcut(CLEAN_POINTCUT)
    public void logMethod() {
        //No implementation needed
    }

    @Around("logMethod()")
    public Object measuerMethod(ProceedingJoinPoint pjp) throws Throwable {
        var start = System.nanoTime();

        var methodName = pjp.getSignature().getName();

        LOGGER.debug("Requesting method: {} with arguments: {}", methodName, Arrays.toString(pjp.getArgs()));

        var executedMethod = pjp.proceed();

        var execTime = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);

        LOGGER.info("Returning from method: {} arguments: {} after time: {} ms", methodName, executedMethod, execTime);
        return executedMethod;
    }
}
