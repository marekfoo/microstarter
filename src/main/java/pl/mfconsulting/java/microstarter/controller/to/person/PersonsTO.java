package pl.mfconsulting.java.microstarter.controller.to.person;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class PersonsTO implements Serializable {

    public static final transient PersonsTO EMPTY = new PersonsTO();
    private static final long serialVersionUID = 5632371695185907790L;
    private final List<PersonTO> persons;

    private PersonsTO() {
        this.persons = new ArrayList<>();
    }

    PersonsTO(final List<PersonTO> personsToMap) {
        ObjectValidator.notNull(personsToMap, "List of persons cannot be null!");

        this.persons = new ArrayList<>(personsToMap);
    }

    public List<PersonTO> getPersons() {
        return persons;
    }

    @Override
    public String toString() {
        return "PersonsTO{" +
                "persons=" + persons +
                '}';
    }
}
