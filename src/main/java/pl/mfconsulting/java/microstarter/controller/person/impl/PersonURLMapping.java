package pl.mfconsulting.java.microstarter.controller.person.impl;

public final class PersonURLMapping {
    public static final String PERSONS_URL = "/persons";
    public static final String PERSON_ID_URL = PERSONS_URL + "/{login}";

    private PersonURLMapping() {
        throw new IllegalStateException("Utility class!");
    }
}
