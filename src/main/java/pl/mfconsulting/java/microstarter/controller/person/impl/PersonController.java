package pl.mfconsulting.java.microstarter.controller.person.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import pl.mfconsulting.java.microstarter.common.aspects.LogMethod;
import pl.mfconsulting.java.microstarter.controller.person.PersonRestAPI;
import pl.mfconsulting.java.microstarter.controller.to.person.PersonMapper;
import pl.mfconsulting.java.microstarter.controller.to.person.PersonTO;
import pl.mfconsulting.java.microstarter.controller.to.person.PersonsTO;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.service.person.PersonService;
import pl.mfconsulting.java.microstarter.service.person.PersonServiceCache;

@RestController
class PersonController implements PersonRestAPI {

    private final PersonService personService;
    private final PersonServiceCache personServiceCache;

    @Autowired
    PersonController(final PersonService personService, final PersonServiceCache personServiceCache) {
        this.personService = personService;
        this.personServiceCache = personServiceCache;
    }

    @Override
    @LogMethod
    public PersonTO getPerson(final String login) {
        return personServiceCache.findPersonByLogin(Login.of(login)).
                map(PersonMapper::toPersonTO).
                orElse(PersonTO.EMPTY);
    }

    @Override
    @LogMethod
    public PersonsTO getPersonsWithAddress() {
        var persons = personService.getAllPersonsWithAddresses();

        if (persons.isEmpty()) {
            return PersonsTO.EMPTY;
        }

        return PersonMapper.toPersonsTO(persons);
    }

    @Override
    @LogMethod
    public PersonTO addPerson(final PersonTO personToAdd) {
        var person = personService.addPerson(PersonMapper.toPerson(personToAdd)).
                orElse(Person.EMPTY);

        return PersonMapper.toPersonTO(person);
    }

    @Override
    @LogMethod
    public PersonTO editPerson(final String login, final PersonTO personToEdit) {
        var person = personService.editPerson(Login.of(login), PersonMapper.toPerson(personToEdit)).orElse(Person.EMPTY);

        return PersonMapper.toPersonTO(person);
    }

    @Override
    @LogMethod
    public void deletePerson(final String login) {
        personService.deletePerson(Login.of(login));
    }

    @Override
    @LogMethod
    public void deletePersons() {
        personService.deletePersons();
    }

}
