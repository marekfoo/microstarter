package pl.mfconsulting.java.microstarter.controller.handler.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;

@ControllerAdvice
class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(DataAccessException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public final ErrorBody handleBusinessError(
            DataAccessException ex, WebRequest request) {
        LOGGER.debug("Handling database handler:{}", ex.getMessage());

        var bodyOfResponse = ex.getMessage();
        var errorBody = new ErrorBody(HttpStatus.UNPROCESSABLE_ENTITY.value(),
                bodyOfResponse);

        LOGGER.debug("Exception returned: {}", errorBody);
        return errorBody;
    }

    @ResponseBody
    @ExceptionHandler(BusinessRuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ErrorBody handleBusinessError(
            BusinessRuntimeException ex, WebRequest request) {
        LOGGER.debug("Handling business error: {}", ex);

        var bodyOfResponse = ex.getMessage();
        var errorBody = new ErrorBody(HttpStatus.INTERNAL_SERVER_ERROR.value(), bodyOfResponse);

        LOGGER.debug("Exception returned in response: {}", errorBody);
        return errorBody;
    }
}
