package pl.mfconsulting.java.microstarter.controller.swagger;

import com.google.common.base.Predicate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.and;
import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Configuration
@ConfigurationProperties(prefix = "microstarter.swagger")
public class SwaggerConfig {
    private static final String NO_ERROR_ENDPOINT_REGEX = "(?!.*error).*$";

    private String title;

    private String version;

    private String description;

    private String contactName;

    private String contactAddress;

    private String contactUrl;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public void setContactUrl(String contactUrl) {
        this.contactUrl = contactUrl;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(excludedPaths())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(this.title)
                .description(this.description)
                .version(this.version)
                .contact(new Contact(this.contactName, this.contactUrl, this.contactAddress))
                .build();
    }

    private Predicate<String> excludedPaths() {
        return and(
                regex(NO_ERROR_ENDPOINT_REGEX)
        );
    }

}
