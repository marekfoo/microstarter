package pl.mfconsulting.java.microstarter.controller.to.address.validation;

import pl.mfconsulting.java.microstarter.controller.to.address.validation.impl.AddressValidation;

import javax.validation.Constraint;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {AddressValidation.class})
@ReportAsSingleViolation
public @interface AddressValidator {
    /**
     * This is the key to message will that will be looked in ValidationMessages.properties for validation
     * errors
     *
     * @return the string
     */
    String message() default "{msg.error.client.validation}";

    Class[] groups() default {};

    Class[] payload() default {};
}
