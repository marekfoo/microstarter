package pl.mfconsulting.java.microstarter.controller.to.address;

import org.hibernate.validator.constraints.Length;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

public class AddressTO implements Serializable {
    public static final transient AddressTO EMPTY = new AddressTO();
    private static final long serialVersionUID = 5415901050164571059L;
    @Length(max = 30, message = "Street max size is 30!")
    @NotEmpty(message = "Street can't be empty!")
    private String street;

    @Length(max = 30, message = "City max size is 50!")
    @NotEmpty(message = "City can't be empty!")
    private String city;

    @Length(max = 30, message = "NZip code max size is 50!")
    @NotEmpty(message = "Zip code can't be empty!")
    private String postalCode;

    AddressTO() {
        this.street = "";
        this.city = "";
        this.postalCode = "";
    }

    AddressTO(final AddressTO address) {
        ObjectValidator.notNull(address, "AddressTO object has to be not null!");

        this.street = address.getStreet();
        this.city = address.getCity();
        this.postalCode = address.getPostalCode();
    }

    AddressTO(final String street, final String city, final String postalCode) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressTO clientTO = (AddressTO) o;
        return Objects.equals(street, clientTO.street) &&
                Objects.equals(city, clientTO.city) &&
                Objects.equals(postalCode, clientTO.postalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, city, postalCode);
    }

    @Override
    public String toString() {
        return "AddressTO{" +
                "street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
