package pl.mfconsulting.java.microstarter.controller.diagnostic;

final class DiagnosticURLMapping {
    static final String THREAD_INFO = "/threadinfo";
    static final String MONITOR_INFO = "/monitor";

    private DiagnosticURLMapping() {
        throw new IllegalStateException("Utility class!");
    }
}
