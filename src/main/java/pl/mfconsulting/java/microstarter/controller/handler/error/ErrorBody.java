package pl.mfconsulting.java.microstarter.controller.handler.error;

import java.io.Serializable;

public final class ErrorBody implements Serializable {
    private static final long serialVersionUID = 1800094457240000613L;

    private int code;
    private String message;

    ErrorBody(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorBody{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
