package pl.mfconsulting.java.microstarter.controller.to.person.validation;

import javax.validation.Constraint;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static pl.mfconsulting.java.microstarter.common.constants.RegexPatterns.LOGIN_CHARACTERS;

@Pattern(regexp = LOGIN_CHARACTERS)
@Target({METHOD, FIELD, PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = {})
@ReportAsSingleViolation
public @interface LoginConstraint {
    String message() default "Invalid login format!";

    Class<?>[] groups() default {};

    Class[] payload() default {};
}
