package pl.mfconsulting.java.microstarter.controller.handler.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.mfconsulting.java.microstarter.common.aspects.LogMethod;

import static pl.mfconsulting.java.microstarter.controller.handler.controller.ErrorURLMapping.CALL_ERROR_URL;
import static pl.mfconsulting.java.microstarter.controller.handler.controller.ErrorURLMapping.ERROR_URL;

@RestController
class ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);

    @GetMapping(value = ERROR_URL)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @LogMethod
    public String errorPage() {
        LOGGER.info("Error page has been called.");

        return "This is Error Page of micro-starter for not found resources.";
    }

    @GetMapping(value = CALL_ERROR_URL)
    @LogMethod
    public void callError() {
        if (true)
            throw new NullPointerException("Errors thrown for test 500 only!");
    }
}
