package pl.mfconsulting.java.microstarter.controller.to.person;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressMapper;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class PersonMapper {
    private PersonMapper() {
    }

    public static Person toPerson(final PersonTO fromObject) {
        ObjectValidator.notNull(fromObject, "PersonTO cannot be null!");

        PersonBuilder personBuilder = new PersonBuilder(Login.of(fromObject.getLogin()));
        personBuilder.firstName(fromObject.getFirstName());
        personBuilder.lastName(fromObject.getLastName());
        return personBuilder.build();
    }

    public static PersonTO toPersonTO(final Person fromObject) {
        ObjectValidator.notNull(fromObject, "Person cannot be null!");

        PersonTOBuilder personBuilder = new PersonTOBuilder(fromObject.getLogin().value());
        personBuilder.firstName(fromObject.getFirstName());
        personBuilder.lastName(fromObject.getLastName());

        personBuilder.addresses(AddressMapper.toAddressTO(fromObject.getAddresses()));

        return personBuilder.build();
    }

    public static PersonsTO toPersonsTO(final Set<Person> personsToMap) {
        ObjectValidator.notNull(personsToMap, "Persons cannot be null!");

        List<PersonTO> persons = new ArrayList<>(personsToMap.size());
        personsToMap.forEach(person -> persons.add(PersonMapper.toPersonTO(person)));

        return new PersonsTO(persons);
    }

}
