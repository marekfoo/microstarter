package pl.mfconsulting.java.microstarter.controller.to.address.validation.impl;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressTO;
import pl.mfconsulting.java.microstarter.controller.to.address.validation.AddressValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressValidation implements ConstraintValidator<AddressValidator, AddressTO> {
    @Override
    public void initialize(final AddressValidator constraintAnnotation) {
        // Do nothing
    }

    @Override
    public boolean isValid(final AddressTO addressTO, final ConstraintValidatorContext constraintValidatorContext) {
        ObjectValidator.validateConstraints(addressTO);

        return true;
    }
}