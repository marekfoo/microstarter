package pl.mfconsulting.java.microstarter.controller.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressMapper;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressTO;
import pl.mfconsulting.java.microstarter.controller.to.address.validation.AddressValidator;
import pl.mfconsulting.java.microstarter.controller.to.person.validation.LoginConstraint;
import pl.mfconsulting.java.microstarter.persistence.address.Address;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.service.address.AddressService;

import static pl.mfconsulting.java.microstarter.controller.address.AddressURL.PERSON_ADDRESS_URL;

@RestController
@Validated
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @PutMapping(value = PERSON_ADDRESS_URL)
    @ResponseStatus(value = HttpStatus.OK)
    public AddressTO addAddressToClient(@PathVariable @LoginConstraint final String login, @RequestBody @AddressValidator final AddressTO address) {
        var addressAdded = addressService.addAddressToClient(Login.of(login), AddressMapper.toAddress(address)).
                orElse(Address.EMPTY);

        return AddressMapper.toAddressTO(addressAdded);
    }
}

