package pl.mfconsulting.java.microstarter.controller.diagnostic;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.mfconsulting.java.microstarter.controller.diagnostic.service.ThreadInfoService;
import pl.mfconsulting.java.microstarter.controller.diagnostic.to.ThreadInfoTO;

import java.util.List;

import static pl.mfconsulting.java.microstarter.controller.diagnostic.DiagnosticURLMapping.THREAD_INFO;

@RestController
public class ThreadInfoController {

    @GetMapping(value = THREAD_INFO)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public List<ThreadInfoTO> getThreadInfo() {
        return ThreadInfoService.getThreadInfo();
    }
}
