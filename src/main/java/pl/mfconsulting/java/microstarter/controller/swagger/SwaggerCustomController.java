package pl.mfconsulting.java.microstarter.controller.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SwaggerCustomController {
    private static final String SWAGGER_URL = "redirect:/swagger-ui.html";

    @GetMapping(value = "${microstarter.swagger.path}")
    public String getSwaggerRestAPI() {
        return SWAGGER_URL;
    }
}
