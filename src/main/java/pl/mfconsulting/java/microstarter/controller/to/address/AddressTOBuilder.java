package pl.mfconsulting.java.microstarter.controller.to.address;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

public final class AddressTOBuilder {
    private final AddressTO address;

    public AddressTOBuilder(final AddressTO address) {
        ObjectValidator.notNull(address, "AddressTO object has to be not null!");

        this.address = new AddressTO(address);
    }

    public AddressTOBuilder() {
        this.address = new AddressTO();
    }

    public AddressTOBuilder city(final String city) {
        this.address.setCity(city);
        return this;
    }

    public AddressTOBuilder street(String street) {
        this.address.setStreet(street);
        return this;
    }

    public AddressTOBuilder postalCode(String postalCode) {
        this.address.setPostalCode(postalCode);
        return this;
    }

    public AddressTO build() {
        return this.address;
    }
}
