package pl.mfconsulting.java.microstarter.controller.to.person;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressTO;

import java.util.Set;

public final class PersonTOBuilder {
    private final PersonTO person;

    public PersonTOBuilder(final PersonTO person) {
        ObjectValidator.notNull(person, "PersonTO object has to be not null!");

        this.person = new PersonTO(person);
    }

    public PersonTOBuilder(final String login) {
        this.person = new PersonTO(login);
    }

    public PersonTOBuilder firstName(final String firstName) {
        this.person.setFirstName(firstName);
        return this;
    }

    public PersonTOBuilder lastName(String lastName) {
        this.person.setLastName(lastName);
        return this;
    }

    public PersonTOBuilder addresses(Set<AddressTO> toAddressTO) {
        this.person.setAddresses(toAddressTO);
        return this;
    }

    public PersonTO build() {
        return this.person;
    }
}
