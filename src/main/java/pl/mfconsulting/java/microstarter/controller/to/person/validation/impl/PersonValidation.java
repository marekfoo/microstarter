package pl.mfconsulting.java.microstarter.controller.to.person.validation.impl;

import pl.mfconsulting.java.microstarter.controller.to.person.PersonTO;
import pl.mfconsulting.java.microstarter.controller.to.person.validation.PersonValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PersonValidation implements ConstraintValidator<PersonValidator, PersonTO> {
    @Override
    public void initialize(final PersonValidator constraintAnnotation) {
        // Do nothing
    }

    @Override
    public boolean isValid(final PersonTO personTO, final ConstraintValidatorContext constraintValidatorContext) {
        return true;
    }
}