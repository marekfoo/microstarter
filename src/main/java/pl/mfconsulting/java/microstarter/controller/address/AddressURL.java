package pl.mfconsulting.java.microstarter.controller.address;

public final class AddressURL {
    public static final String ADDRESSES_URL = "/addresses";
    public static final String PERSON_ADDRESS_URL = ADDRESSES_URL + "/{login}";

    private AddressURL() {
        throw new IllegalStateException("Utility class!");
    }

}
