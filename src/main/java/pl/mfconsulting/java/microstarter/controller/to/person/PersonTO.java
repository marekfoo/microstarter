package pl.mfconsulting.java.microstarter.controller.to.person;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.controller.to.address.AddressTO;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public final class PersonTO implements Serializable {

    public static final transient PersonTO EMPTY = new PersonTO();
    private static final long serialVersionUID = -4729307554945761317L;
    private final String login;
    private String firstName;
    private String lastName;
    private Set<AddressTO> addresses;

    private PersonTO() {
        this.login = "";
        this.firstName = "";
        this.lastName = "";
        this.addresses = new HashSet<>();
    }

    PersonTO(final PersonTO person) {
        ObjectValidator.notNull(person, "PersonTO object has to be not null!");

        this.login = person.getLogin();
        this.firstName = person.getFirstName();
        this.lastName = person.getLastName();
        this.addresses = person.getAddresses();
    }

    PersonTO(final String login) {
        ObjectValidator.notNull(login, "Login object has to be not null!");

        this.login = login;
        this.firstName = "";
        this.lastName = "";
    }

    public String getFirstName() {
        return firstName;
    }


    void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public Set<AddressTO> getAddresses() {
        return addresses;
    }

    void setAddresses(Set<AddressTO> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "PersonTO{" +
                "login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
