package pl.mfconsulting.java.microstarter.controller.diagnostic.to;

public final class ThreadInfoTOBuilder {
    private final ThreadInfoTO threadInfoTO;

    public ThreadInfoTOBuilder(String threadName) {
        this.threadInfoTO = new ThreadInfoTO(threadName);
    }

    public ThreadInfoTOBuilder threadState(final String name) {
        this.threadInfoTO.setThreadState(name);
        return this;
    }

    public ThreadInfoTOBuilder blockedCount(final long count) {
        this.threadInfoTO.setBlockedCount(count);
        return this;
    }

    public ThreadInfoTOBuilder blockedTime(final long time) {
        this.threadInfoTO.setBlockedTime(time);
        return this;
    }

    public ThreadInfoTOBuilder waitedCount(final long count) {
        this.threadInfoTO.setWaitedCount(count);
        return this;
    }

    public ThreadInfoTOBuilder waitedTime(final long time) {
        this.threadInfoTO.setWaitedTime(time);
        return this;
    }

    public ThreadInfoTO build() {
        return this.threadInfoTO;
    }
}
