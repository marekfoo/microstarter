package pl.mfconsulting.java.microstarter.controller.handler.controller;

final class ErrorURLMapping {
    static final String ERROR_URL = "/error";
    static final String CALL_ERROR_URL = "/callerror";

    private ErrorURLMapping() {
        throw new IllegalStateException("Utility class!");
    }
}
