package pl.mfconsulting.java.microstarter.controller.diagnostic.to;

import java.io.Serializable;

public final class ThreadInfoTO implements Serializable {
    private static final long serialVersionUID = -2074719700873039131L;
    private String threadName;
    private String threadState;
    private long blockedCount;
    private long blockedTime;
    private long waitedCount;
    private long waitedTime;

    public ThreadInfoTO(final ThreadInfoTO threadInfoTO) {
        this.threadName = threadInfoTO.threadName;
        this.blockedCount = threadInfoTO.blockedCount;
        this.blockedTime = threadInfoTO.blockedTime;
        this.waitedCount = threadInfoTO.waitedCount;
        this.waitedTime = threadInfoTO.waitedTime;
        this.threadState = threadInfoTO.threadState;
    }

    ThreadInfoTO(final String threadName) {
        this.threadName = threadName;
    }

    public String getThreadState() {
        return threadState;
    }

    void setThreadState(String threadState) {
        this.threadState = threadState;
    }

    public String getThreadName() {
        return threadName;
    }

    void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public long getBlockedCount() {
        return blockedCount;
    }

    void setBlockedCount(long blockedCount) {
        this.blockedCount = blockedCount;
    }

    public long getBlockedTime() {
        return blockedTime;
    }

    void setBlockedTime(long blockedTime) {
        this.blockedTime = blockedTime;
    }

    public long getWaitedCount() {
        return waitedCount;
    }

    void setWaitedCount(long waitedCount) {
        this.waitedCount = waitedCount;
    }

    public long getWaitedTime() {
        return waitedTime;
    }

    void setWaitedTime(long waitedTime) {
        this.waitedTime = waitedTime;
    }

    @Override
    public String toString() {
        return "ThreadInfoTO{" +
                "threadName='" + threadName + '\'' +
                ", threadState='" + threadState + '\'' +
                ", blockedCount=" + blockedCount +
                ", blockedTime=" + blockedTime +
                ", waitedCount=" + waitedCount +
                ", waitedTime=" + waitedTime +
                '}';
    }
}
