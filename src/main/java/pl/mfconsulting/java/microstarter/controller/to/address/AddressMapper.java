package pl.mfconsulting.java.microstarter.controller.to.address;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.persistence.address.*;

import java.util.Set;
import java.util.stream.Collectors;

public final class AddressMapper {
    private AddressMapper() {
    }

    public static Address toAddress(final AddressTO fromObject) {
        ObjectValidator.notNull(fromObject, "AddressTO cannot be null!");

        return new AddressBuilder().city(City.of(fromObject.getCity()))
                .postCode(PostCode.of(fromObject.getPostalCode())).
                        street(Street.of(fromObject.getStreet())).build();
    }

    public static AddressTO toAddressTO(final Address fromObject) {
        ObjectValidator.notNull(fromObject, "Address cannot be null!");
        return new AddressTOBuilder().city(fromObject.getCity().value()).
                street(fromObject.getStreet().value()).
                postalCode(fromObject.getPostCode().value())
                .build();
    }

    public static Set<AddressTO> toAddressTO(final Set<Address> fromObject) {
        ObjectValidator.notNull(fromObject, "Address cannot be null!");

        return fromObject.stream().map(AddressMapper::toAddressTO).collect(Collectors.toSet());
    }

}
