package pl.mfconsulting.java.microstarter.controller.person;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.mfconsulting.java.microstarter.controller.to.person.PersonTO;
import pl.mfconsulting.java.microstarter.controller.to.person.PersonsTO;
import pl.mfconsulting.java.microstarter.controller.to.person.validation.LoginConstraint;
import pl.mfconsulting.java.microstarter.controller.to.person.validation.PersonValidator;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static pl.mfconsulting.java.microstarter.controller.person.impl.PersonURLMapping.PERSONS_URL;
import static pl.mfconsulting.java.microstarter.controller.person.impl.PersonURLMapping.PERSON_ID_URL;

@Validated
public interface PersonRestAPI {

    @GetMapping(value = PERSON_ID_URL)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    PersonTO getPerson(@PathVariable @LoginConstraint final String login);

    @GetMapping(value = PERSONS_URL)
    @ResponseStatus(value = HttpStatus.OK)
    PersonsTO getPersonsWithAddress();

    @PostMapping(value = PERSONS_URL, consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    PersonTO addPerson(@RequestBody @PersonValidator final PersonTO personToAdd);

    @PutMapping(value = PERSON_ID_URL, consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    PersonTO editPerson(@PathVariable @LoginConstraint final String login,
                        @RequestBody @PersonValidator final PersonTO personToEdit);

    @DeleteMapping(value = PERSON_ID_URL)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    void deletePerson(@PathVariable @LoginConstraint final String login);

    @DeleteMapping(value = PERSONS_URL)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    void deletePersons();
}
