package pl.mfconsulting.java.microstarter.controller.diagnostic.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mfconsulting.java.microstarter.controller.diagnostic.to.ThreadInfoTO;
import pl.mfconsulting.java.microstarter.controller.diagnostic.to.ThreadInfoTOBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.List;

public final class ThreadInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadInfoService.class);

    private ThreadInfoService() {
        throw new IllegalStateException("Utility class!");
    }

    public static List<ThreadInfoTO> getThreadInfo() {
        ThreadMXBean mxBean = ManagementFactory.getThreadMXBean();
        long[] threadIds = mxBean.getAllThreadIds();
        ThreadInfo[] threadInfos =
                mxBean.getThreadInfo(threadIds);

        List<ThreadInfoTO> threadInfosToReturn = new ArrayList<>(threadIds.length);

        for (ThreadInfo threadInfo : threadInfos) {
            ThreadInfoTO threadInfoTO = new ThreadInfoTOBuilder(threadInfo.getThreadName() + " id:" + threadInfo.getThreadId()).
                    threadState(threadInfo.getThreadState().name()).
                    blockedCount(threadInfo.getBlockedCount()).
                    blockedTime(threadInfo.getBlockedTime()).
                    waitedCount(threadInfo.getWaitedCount()).
                    waitedTime(threadInfo.getWaitedTime()).
                    build();
            threadInfosToReturn.add(threadInfoTO);
        }

        LOGGER.info("Threads state: {}", threadInfosToReturn);
        return threadInfosToReturn;
    }
}
