package pl.mfconsulting.java.microstarter.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import pl.mfconsulting.java.microstarter.repository.AddressRepository;
import pl.mfconsulting.java.microstarter.repository.PersonRepository;
import pl.mfconsulting.java.microstarter.service.address.AddressService;
import pl.mfconsulting.java.microstarter.service.address.impl.AddressServiceImpl;
import pl.mfconsulting.java.microstarter.service.person.PersonService;
import pl.mfconsulting.java.microstarter.service.person.PersonServiceAsync;
import pl.mfconsulting.java.microstarter.service.person.PersonServiceCache;
import pl.mfconsulting.java.microstarter.service.person.impl.PersonServiceAsyncImpl;
import pl.mfconsulting.java.microstarter.service.person.impl.PersonServiceCacheImpl;
import pl.mfconsulting.java.microstarter.service.person.impl.PersonServiceImpl;

import java.util.concurrent.Executor;

@Configuration
@ComponentScan(value = "pl.mfconsulting.java.microstarter")
@EnableAsync
@EnableAspectJAutoProxy
class ConfigurationService {

    @Bean(name = "findPersonExecutor")
    public Executor findPersonExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(20);
        executor.setThreadNamePrefix("Async Thread-");
        executor.initialize();
        return executor;
    }

    @Bean
    public PersonService getPersonService(final PersonRepository personRepository, final AddressRepository addressRepository) {
        return new PersonServiceImpl(personRepository, addressRepository);
    }

    @Bean
    public AddressService getAddressService(final PersonRepository personRepository, final AddressRepository addressRepository) {
        return new AddressServiceImpl(addressRepository, personRepository);
    }

    @Bean
    public PersonServiceAsync getPersonServiceAsync(final PersonService personService) {
        return new PersonServiceAsyncImpl(personService);
    }

    @Bean
    public PersonServiceCache getPersonCache(final PersonServiceAsync personService) {
        return new PersonServiceCacheImpl(personService);
    }

}
