package pl.mfconsulting.java.microstarter.security.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import pl.mfconsulting.java.microstarter.security.filters.BasicWebFilter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String USER = "admin";
    private static final String USER_PASS = "admin";
    private static final String ROLE_USER = "USER";


    private final CustomBasicAuthenticationEntryPoint authenticationEntryPoint;
    private final AuthenticationManagerBuilder authenticationManager;

    @Autowired
    public SecurityConfiguration(CustomBasicAuthenticationEntryPoint authenticationEntryPoint, AuthenticationManagerBuilder authenticationManager) {
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.authenticationManager = authenticationManager;
    }

    @PostConstruct
    public void configureUserAuthentication() throws Exception {
        authenticationManager
                .inMemoryAuthentication()
                .withUser(USER)
                .password(passwordEncoder().encode(USER_PASS))
                .authorities(ROLE_USER)
        ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .authenticationEntryPoint(authenticationEntryPoint);

        http.addFilterAfter(new BasicWebFilter(), BasicAuthenticationFilter.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
