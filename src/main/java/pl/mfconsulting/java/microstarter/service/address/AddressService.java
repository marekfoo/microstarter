package pl.mfconsulting.java.microstarter.service.address;

import pl.mfconsulting.java.microstarter.persistence.address.Address;
import pl.mfconsulting.java.microstarter.persistence.person.Login;

import java.util.Optional;

public interface AddressService {

    Optional<Address> addAddressToClient(Login login, Address address);

    void deleteAddresses();
}
