package pl.mfconsulting.java.microstarter.service.address.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.persistence.address.Address;
import pl.mfconsulting.java.microstarter.persistence.address.AddressBuilder;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.repository.AddressRepository;
import pl.mfconsulting.java.microstarter.repository.PersonRepository;
import pl.mfconsulting.java.microstarter.service.address.AddressService;

import java.util.Optional;

public class AddressServiceImpl implements AddressService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressServiceImpl.class);

    private final AddressRepository addressRepository;
    private final PersonRepository personRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository, PersonRepository personRepository) {
        this.addressRepository = addressRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Optional<Address> addAddressToClient(final Login login, final Address address) {
        ObjectValidator.notNull(login, "Login cannot be null");
        ObjectValidator.notNull(address, "Address cannot be null");
        LOGGER.debug("Adding {} to client {}", login, address);

        var addressFound = addressRepository.findByAddressParts(address.getStreet(), address.getCity(), address.getPostCode());

        if (addressFound.isPresent() && addressFound.get().getPerson().getLogin().equals(login)) {
            LOGGER.info("Address {} already added to client {}", login, address);

            return addressFound;
        }

        var client = personRepository.findByLogin(login);

        return client.map(value ->
                addressRepository.save(new AddressBuilder(address).person(value).build()))
                .or(() -> {
                    LOGGER.warn("Client {} not found to be added to address {}", login, address);
                    return Optional.of(Address.EMPTY);
                });
    }

    @Override
    public void deleteAddresses() {
        addressRepository.deleteAll();
    }

}
        