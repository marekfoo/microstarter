package pl.mfconsulting.java.microstarter.service.person;

import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PersonService {
    Optional<Person> findPersonByLogin(final Login login);

    List<Person> getAllPersons();

    Set<Person> getAllPersonsWithAddresses();

    Optional<Person> addPerson(final Person person);

    void deletePerson(final Login personToDelete);

    Optional<Person> editPerson(final Login of, Person person);

    void deletePersons();
}
