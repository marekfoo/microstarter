package pl.mfconsulting.java.microstarter.service.person;

import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;

import java.util.Optional;

/**
 * Supports fetching data for the resource about the same id in case of parallel requests.
 */
public interface PersonServiceCache {

    /**
     * If many requests will request the same person at once,
     * only ones it will be fetched from the database.
     */
    Optional<Person> findPersonByLogin(final Login login);
}
