package pl.mfconsulting.java.microstarter.service.person.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.service.person.PersonService;
import pl.mfconsulting.java.microstarter.service.person.PersonServiceAsync;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class PersonServiceAsyncImpl implements PersonServiceAsync {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceAsyncImpl.class);

    private final PersonService personService;

    @Autowired
    public PersonServiceAsyncImpl(final PersonService personService) {
        this.personService = personService;
    }

    @Async("findPersonExecutor")
    @Override
    public CompletableFuture<Optional<Person>> findPersonByLogin(final Login login) {
        ObjectValidator.notNull(login, "Login can't be empty!");

        LOGGER.debug("Calling find person in async way for {}", login);

        Optional<Person> person = personService.findPersonByLogin(login);

        return CompletableFuture.completedFuture(person);
    }

}
