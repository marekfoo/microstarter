package pl.mfconsulting.java.microstarter.service.person.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.service.person.PersonServiceAsync;
import pl.mfconsulting.java.microstarter.service.person.PersonServiceCache;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

public class PersonServiceCacheImpl implements PersonServiceCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceCacheImpl.class);
    private static final Map<Login, CompletableFuture<Optional<Person>>> personsCache = new ConcurrentHashMap<>();
    private final PersonServiceAsync personServiceAsync;

    @Autowired
    public PersonServiceCacheImpl(final PersonServiceAsync personServiceAsync) {
        this.personServiceAsync = personServiceAsync;
    }

    @Override
    public Optional<Person> findPersonByLogin(final Login login) {
        LOGGER.debug("Getting person from cache for {}", login);

        try {
            return personsCache.computeIfAbsent(login, this::find).get();

        } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
            throw new BusinessRuntimeException("Getting person failed for: " + login, e);
        } catch (Exception e) {
            throw new BusinessRuntimeException("Getting person from database failed for " + login, e);
        }
    }

    private CompletableFuture<Optional<Person>> find(final Login loginToFind) {
        return personServiceAsync.findPersonByLogin(loginToFind).
                whenComplete((foundPerson, throwable) ->
                        personsCache.remove(loginToFind)
                );
    }
}
