package pl.mfconsulting.java.microstarter.service.person.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.mfconsulting.java.microstarter.common.exception.BusinessRuntimeException;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.persistence.address.Address;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;
import pl.mfconsulting.java.microstarter.persistence.person.PersonBuilder;
import pl.mfconsulting.java.microstarter.repository.AddressRepository;
import pl.mfconsulting.java.microstarter.repository.PersonRepository;
import pl.mfconsulting.java.microstarter.service.person.PersonService;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class PersonServiceImpl implements PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    private final PersonRepository personRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public PersonServiceImpl(final PersonRepository personRepository, AddressRepository addressRepository) {
        this.personRepository = personRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    public Optional<Person> findPersonByLogin(final Login login) {
        ObjectValidator.notNull(login, "Login can't be empty!");

        LOGGER.debug("Looking for a Person: {}...", login);

        return personRepository.findByLogin(login);
    }

    @Override
    public List<Person> getAllPersons() {
        LOGGER.debug("Looking for all Persons...");
        return personRepository.findAll();
    }

    @Override
    public Set<Person> getAllPersonsWithAddresses() {
        LOGGER.debug("Looking for all Persons with addresses...");

        return personRepository.findAll().stream().
                map(person ->
                        new PersonBuilder(person, getAddresses(person)).build())
                .collect(Collectors.toSet());
    }

    private Set<Address> getAddresses(Person client) {
        return addressRepository.findAddressesByClient(client.getLogin());
    }

    @Override
    public Optional<Person> addPerson(final Person person) {
        ObjectValidator.notNull(person, "Person cannot be empty!");
        LOGGER.debug("Adding new person {}...", person);

        personRepository.findByLogin(person.getLogin()).
                ifPresent(person1 -> {
                    throw new BusinessRuntimeException("Person already exists about " + person1.getLogin());
                });

        return Optional.of(personRepository.save(person));
    }

    @Override
    public void deletePerson(final Login personToDelete) {
        ObjectValidator.notNull(personToDelete, "Person login cannot be empty!");

        LOGGER.debug("Deleting Person {}...", personToDelete);

        var person = personRepository.findByLogin(personToDelete).
                orElseThrow(() -> new BusinessRuntimeException("Missing Person to delete!"));

        personRepository.delete(person);
    }

    @Override
    public Optional<Person> editPerson(final Login login, final Person personToEdit) {
        ObjectValidator.notNull(personToEdit, "Person can't be empty!");

        LOGGER.debug("Editing Person {}...", personToEdit);

        var person = personRepository.findByLogin(login).
                orElseThrow(() -> new BusinessRuntimeException("Missing Person to edit!"));

        var changedPerson =
                new PersonBuilder(person).firstName(personToEdit.getFirstName()).lastName(personToEdit.getLastName()).build();

        return Optional.of(personRepository.save(changedPerson));
    }

    @Override
    public void deletePersons() {
        LOGGER.debug("Deleting all persons...");

        personRepository.deleteAll();
    }
}
