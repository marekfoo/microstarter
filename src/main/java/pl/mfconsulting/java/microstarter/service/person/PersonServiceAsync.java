package pl.mfconsulting.java.microstarter.service.person;

import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * This is Async version for PersonService but with implementation for only selected methods.
 */
public interface PersonServiceAsync {
    CompletableFuture<Optional<Person>> findPersonByLogin(final Login login);
}
