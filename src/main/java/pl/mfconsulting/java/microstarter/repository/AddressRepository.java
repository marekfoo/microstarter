package pl.mfconsulting.java.microstarter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.mfconsulting.java.microstarter.persistence.address.Address;
import pl.mfconsulting.java.microstarter.persistence.address.City;
import pl.mfconsulting.java.microstarter.persistence.address.PostCode;
import pl.mfconsulting.java.microstarter.persistence.address.Street;
import pl.mfconsulting.java.microstarter.persistence.person.Login;

import java.util.Optional;
import java.util.Set;

public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query("SELECT u FROM Address u WHERE u.postCode=:postCode and u.street=:street and u.city=:city")
    Optional<Address> findByAddressParts(@Param("street") final Street street, @Param("city") final City city,
                                         @Param("postCode") final PostCode postCode);

    @Query("SELECT u FROM Address u join Person cl on cl = u.person where cl.login=:login ")
    Set<Address> findAddressesByClient(@Param("login") final Login login);

}
