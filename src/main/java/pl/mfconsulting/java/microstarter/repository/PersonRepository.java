package pl.mfconsulting.java.microstarter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.mfconsulting.java.microstarter.persistence.person.Login;
import pl.mfconsulting.java.microstarter.persistence.person.Person;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("SELECT u FROM Person u WHERE u.login=:login")
    Optional<Person> findByLogin(@Param("login") final Login login);
}
