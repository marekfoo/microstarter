package pl.mfconsulting.java.microstarter.persistence.contactdata;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity(name = "CONTACT_DATA")
@SequenceGenerator(name = "contact_data_seq", sequenceName = "contact_data_id_seq", initialValue = 100, allocationSize = 1)
public class ContactData implements Serializable {

    private static final long serialVersionUID = -7309765650654304545L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_data_seq")
    private long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "MOBILE_PHONE")
    private String mobilePhone;

    protected ContactData() {
    }

    public ContactData(final String email, final String mobilePhone) {
        this.email = email;
        this.mobilePhone = mobilePhone;
    }
}
