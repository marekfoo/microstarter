package pl.mfconsulting.java.microstarter.persistence.address;


import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Embeddable
public class PostCode implements Serializable {
    static final transient PostCode EMPTY = new PostCode();
    private static final long serialVersionUID = -3746716151525305287L;
    private static final transient Map<String, PostCode> values = Collections.synchronizedMap(new HashMap<>());
    @Column(name = "POST_CODE", length = 50)
    @Length(max = 30, message = "Zip code max size is 50!")
    @NotEmpty(message = "Zip code can't be empty!")
    private String value;

    private PostCode() {
        this.value = "";
    }

    private PostCode(final String value) {
        this.value = value;
    }

    public static PostCode of(final String value) {
        return addToValues(value);

    }

    private static PostCode addToValues(final String value) {
        return values.computeIfAbsent(value, PostCode::apply);
    }

    private static PostCode apply(String value) {
        PostCode login = new PostCode(value);
        ObjectValidator.validateConstraints(login);

        return login;
    }

    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return "PostCode='" + value + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCode value1 = (PostCode) o;
        return Objects.equals(value, value1.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
