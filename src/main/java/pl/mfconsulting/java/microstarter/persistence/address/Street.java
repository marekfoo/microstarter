package pl.mfconsulting.java.microstarter.persistence.address;


import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Embeddable
public class Street implements Serializable {
    static final transient Street EMPTY = new Street();
    private static final long serialVersionUID = -3746716151525305287L;
    private static final transient Map<String, Street> values = Collections.synchronizedMap(new HashMap<>());

    @Column(name = "STREET", length = 50)
    @Length(max = 30, message = "Street max size is 50!")
    @NotEmpty(message = "Street can't be empty!")
    private String value;

    private Street() {
        this.value = "";
    }

    private Street(final String value) {
        this.value = value;
    }

    public static Street of(final String value) {
        return addToValues(value);

    }

    private static Street addToValues(final String value) {
        return values.computeIfAbsent(value, Street::apply);
    }

    private static Street apply(String value) {
        Street login = new Street(value);
        ObjectValidator.validateConstraints(login);

        return login;
    }

    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Street='" + value + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Street value1 = (Street) o;
        return Objects.equals(value, value1.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
