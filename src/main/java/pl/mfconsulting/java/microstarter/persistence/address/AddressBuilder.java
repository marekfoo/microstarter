package pl.mfconsulting.java.microstarter.persistence.address;


import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.persistence.person.Person;

public class AddressBuilder {
    private final Address address;

    public AddressBuilder(final Address address) {
        ObjectValidator.notNull(address, "Address object has to be not null!");

        this.address = new Address(address, null);
    }

    public AddressBuilder() {
        this.address = new Address();
    }

    public AddressBuilder city(final City city) {
        this.address.setCity(city);
        return this;
    }

    public AddressBuilder postCode(final PostCode postCode) {
        this.address.setPostCode(postCode);
        return this;
    }

    public AddressBuilder street(final Street street) {
        this.address.setStreet(street);
        return this;
    }

    public AddressBuilder person(final Person person) {
        this.address.setPerson(person);
        return this;
    }


    public Address build() {
        ObjectValidator.validateConstraints(address);

        return this.address;
    }
}
