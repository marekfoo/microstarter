package pl.mfconsulting.java.microstarter.persistence.role;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

@Entity(name = "ROLE")
@SequenceGenerator(name = "role_seq", sequenceName = "role_id_seq", initialValue = 100, allocationSize = 1)
public class Role implements Serializable {

    private static final long serialVersionUID = -2521116682097521265L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_seq")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "RESOURCE_NAME")
    private String resourceName;

    protected Role() {
    }

    public Role(final String name, final String resourceName) {
        this.name = name;
        this.resourceName = resourceName;
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", resourceName='" + resourceName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(name, role.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
