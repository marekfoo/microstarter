package pl.mfconsulting.java.microstarter.persistence.person;

import jakarta.persistence.*;
import org.hibernate.validator.constraints.Length;
import pl.mfconsulting.java.microstarter.persistence.address.Address;
import pl.mfconsulting.java.microstarter.persistence.contactdata.ContactData;
import pl.mfconsulting.java.microstarter.persistence.role.Role;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "person", schema = "public")
@SequenceGenerator(name = "person_seq", sequenceName = "person_id_seq", initialValue = 100, allocationSize = 1)
public class Person implements Serializable {

    public static final transient Person EMPTY = new Person();
    private static final long serialVersionUID = -1249677431973343368L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_seq")
    private long id;

    @Embedded
    @Column(unique = true)
    private Login login;

    @Column(name = "FIRST_NAME")
    @Length(max = 50, message = "Name max size is 50!")
    private String firstName;

    @Column(name = "LAST_NAME")
    @Length(max = 50, message = "Last name max size is 50!")
    private String lastName;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ContactData contactData;

    @Transient
    private Set<Address> addresses = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "Person_ROLE", joinColumns = {@JoinColumn(name = "Person_FK")}, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_FK")})
    private Set<Role> role;

    protected Person() {
    }

    Person(final Person person, final Set<Address> addresses) {
        this.id = person.getId();
        this.login = person.getLogin();
        this.firstName = person.getFirstName();
        this.lastName = person.getLastName();

        this.addresses = addresses;
    }

    Person(final Login login) {
        this.login = login;
    }

    long getId() {
        return id;
    }

    public Login getLogin() {
        return this.login;
    }

    public String getFirstName() {
        return firstName;
    }

    void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    @Override
    public String toString() {
        return "Person{" + "id='" + id + '\'' + ", login='" + login + '\'' + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\'' + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Person person = (Person) o;
        return Objects.equals(login, person.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }
}

