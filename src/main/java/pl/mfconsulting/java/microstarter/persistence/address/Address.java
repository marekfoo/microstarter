package pl.mfconsulting.java.microstarter.persistence.address;

import jakarta.persistence.*;
import pl.mfconsulting.java.microstarter.persistence.person.Person;

import java.io.Serializable;
import java.util.Objects;

@Entity(name = "Address")
@SequenceGenerator(name = "address_seq", sequenceName = "address_id_seq", initialValue = 100, allocationSize = 1)
public class Address implements Serializable {
    private static final long serialVersionUID = 5306994592356209180L;

    public static final transient Address EMPTY = new Address();

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq")
    private long id;

    @Embedded
    private Street street;

    @Embedded
    private PostCode postCode;

    @Embedded
    private City city;

    @ManyToOne
    @JoinColumn(name = "PERSON_FK")
    private Person person;

    protected Address() {
        this.street = Street.EMPTY;
        this.city = City.EMPTY;
        this.postCode = PostCode.EMPTY;
    }

    Address(final Address address, final Person person) {
        this.street = address.getStreet();
        this.postCode = address.getPostCode();
        this.city = address.getCity();
        this.person = person;
    }

    Address(final Street street, final PostCode postCode, final City city, final Person person) {
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.person = person;
    }

    public long getId() {
        return id;
    }

    public Street getStreet() {
        return street;
    }

    void setStreet(Street street) {
        this.street = street;
    }

    public PostCode getPostCode() {
        return postCode;
    }

    void setPostCode(PostCode postCode) {
        this.postCode = postCode;
    }

    public City getCity() {
        return city;
    }

    void setCity(City city) {
        this.city = city;
    }

    public Person getPerson() {
        return person;
    }

    void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(street, address.street) &&
                Objects.equals(postCode, address.postCode) &&
                Objects.equals(city, address.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, postCode, city);
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street=" + street +
                ", postCode=" + postCode +
                ", city=" + city +
                '}';
    }
}
