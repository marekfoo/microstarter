package pl.mfconsulting.java.microstarter.persistence.address;


import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Embeddable
public class City implements Serializable {
    static final transient City EMPTY = new City();
    private static final long serialVersionUID = -3746716151525305287L;
    private static final transient Map<String, City> values = Collections.synchronizedMap(new HashMap<>());
    @Column(name = "CITY", length = 50)
    @Length(max = 30, message = "City max size is 50!")
    @NotEmpty(message = "City can't be empty!")
    private String value;

    private City() {
        this.value = "";
    }

    private City(final String value) {
        this.value = value;
    }

    public static City of(final String value) {
        return addToValues(value);

    }

    private static City addToValues(final String value) {
        return values.computeIfAbsent(value, City::apply);
    }

    private static City apply(String value) {
        City login = new City(value);
        ObjectValidator.validateConstraints(login);

        return login;
    }

    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return "City='" + value + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City value1 = (City) o;
        return Objects.equals(value, value1.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
