package pl.mfconsulting.java.microstarter.persistence.person;

import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;
import pl.mfconsulting.java.microstarter.persistence.address.Address;

import java.util.HashSet;
import java.util.Set;

public class PersonBuilder {
    private final Person person;

    public PersonBuilder(final Person person) {
        ObjectValidator.notNull(person, "Person object has to be not null!");

        this.person = new Person(person, new HashSet<>());
    }

    public PersonBuilder(final Person person, final Set<Address> addresses) {
        ObjectValidator.notNull(person, "Person object has to be not null!");

        this.person = new Person(person, addresses);
    }

    public PersonBuilder(final Login login) {
        this.person = new Person(login);
    }

    public PersonBuilder firstName(final String firstName) {
        this.person.setFirstName(firstName);
        return this;
    }

    public PersonBuilder lastName(final String lastName) {
        this.person.setLastName(lastName);
        return this;
    }

    public Person build() {
        ObjectValidator.validateConstraints(person);

        return this.person;
    }
}
