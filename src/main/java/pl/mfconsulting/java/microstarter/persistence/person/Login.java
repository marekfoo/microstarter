package pl.mfconsulting.java.microstarter.persistence.person;


import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import pl.mfconsulting.java.microstarter.common.validator.ObjectValidator;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Embeddable
public class Login implements Serializable {
    private static final long serialVersionUID = -7720165528835412927L;

    static final transient Map<String, Login> logins = Collections.synchronizedMap(new HashMap<>());

    static final transient Login EMPTY = new Login();

    @Column(name = "LOGIN", length = 30)
    @Length(max = 30, message = "Login max size is 30!")
    @NotEmpty(message = "Login can't be empty!")
    private String value;

    private Login() {
    }

    private Login(final String value) {
        this.value = value;
    }

    public static Login of(final String value) {
        return addToLogins(value);

    }

    private static Login addToLogins(final String value) {
        return logins.computeIfAbsent(value, Login::apply);
    }

    private static Login apply(String value) {
        Login login = new Login(value);
        ObjectValidator.validateConstraints(login);

        return login;
    }

    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Login{" +
                "login='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Login login1 = (Login) o;
        return Objects.equals(value, login1.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
