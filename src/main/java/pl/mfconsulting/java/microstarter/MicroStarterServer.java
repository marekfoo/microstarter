package pl.mfconsulting.java.microstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroStarterServer {

    public static void main(String... args) {
        SpringApplication.run(MicroStarterServer.class);
    }
}
