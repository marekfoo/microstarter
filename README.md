
# Micro-starter
Micro-starter is a web-based backend only application that provides CRUD facility for person domain.
You can add|edit|get|getAll person. Not all features of data model has been implemented.
The goal is to have fundamental code which gives possibility to quickly test new solutions and self-learning.

## Getting Started
To get project just do it:
* create bitbucket account
* run git clone https://bitbucket.org/marekfoo/microstarter.git

### Prerequisites
This software you need to install before go
* Virtual box -> only if you are a Windows user.
* Curl
* Gradle
* Vagrant
* GIT
* Intellij Community

### Installing
For Windows users
To install linux take a usage of rest-api existing Vagrant file (configured for fedora) and run commands below:
* vagrant up -> Starts VM, fist time it downloads box. Run it every time you want to start VM.
* vagrant provision --provision-with shell -> perform provisioning for given box, run it once with success.
* vagrant ssh -> access VM using ssh

## REST API
Microstarter app provide list of endpoints to manage persons and addresses which can be
obtained by Swagger tool under link: http://localhost:8090/starter/rest-api

## Running the tests

### Running
When database is up-and-ready then time to compile and run the project
* To create executable jar just run: gradle bootJar:
To start application you have to provide profile dedicated for appropriate stage dev/prod.
Example is: -Dspring.profiles.active=dev which has to be provided in VM args.
* To run in development mode just run the main class MicroStarterServer.
* To re-deploy after code change just press Ctrl+F5 in IntelliJ.
* To update database only just run: gradle task dev update
* To start app using gradle: gradlew bootRun
* To build with tests and start: gradlew build && java -jar build/libs/micro-starter-0.1.0-SNAPSHOT.jar

App is accessible under: http://localhost:8090/persons
Port is set on: 8090.

### Testing
To perform end-to-end tests you can take a usage of scripts from folder "restCalls".
Using Curl, they will call Micro-starter REST API.

### Quality
Sonarcloud has been applied. Run gradle sonarcube task to perform analysis.
Jacoco plugin has been used to perform test coverage.

## Deployment
Using docker
    - ./gradlew build
    - sudo docker-compose build
    - sudo docker-compose up
    - to stop: sudo docker-compose down

## Security
To be able to perform any request to app you need to provide credentials: admin/admin.
All endpoints are secured by basic authentication with implementation of spring security module.

## Built With
* Spring REST
* Spring Boot
* Spring Data
* Liquibase based on yaml
* Postgres
* Gradle
* Java 10

## Contributing
Anyone who what to learn more and add something valuable is welcome.

## Versioning
The [SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags.

## Authors
 * **Marek Furmaniak** - *Initial work* - mfconsulting.pl

## License
You can use, extend and contribute in this project free of charge. It is for education purpose.
It is not for commercial purpose.